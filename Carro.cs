using System;

namespace POO2
{
    class Carro
    {
        public string color;
        public string marca;
        public int anyoModelo;
        public int numeroPuertas;
        public decimal precio;
        public Tanque miTanque;
        public Motor miMotor;
        public decimal litrosIniciales;
        public Carro(){
            miMotor = new Motor();
            miTanque = new Tanque();
        }
        public void acelerar(){
            Console.WriteLine("Estoy acelerando");
            miMotor.consumirCombustible(ref miTanque, (decimal)0.4);
            Console.WriteLine("Remanente de combustible: " + miTanque.obtenerLitros().ToString());
        }

        public void frenar(){
            Console.WriteLine("Estoy frenando");
        }
        public void obtenerCaracteristicas(){
            Console.WriteLine("Caracteristicas del auto");
            Console.WriteLine("Color: " + color);
            Console.WriteLine("Marcar: " + marca);
            Console.WriteLine("Año del modelo: "+ anyoModelo.ToString());
            Console.WriteLine("Número de puertas: " + numeroPuertas.ToString());
            Console.WriteLine("Precio: " + precio.ToString());
            Console.WriteLine("Tipo de Carburador: " + miMotor.obtenerTipoCarburador());
            Console.WriteLine("Número de cilindros: " + miMotor.obtenerNumeroCilindros());
            Console.WriteLine("Tipo de cobustible: "+ miMotor.TipoCombustible);
            
        }
        
    }
}
