﻿using System;

namespace POO2
{
    class Program
    {
        static void Main(string[] args)
        {
            Carro miCarro = new Carro();
            miCarro.color = "plata";
            miCarro.marca = "Toyota";
            miCarro.numeroPuertas = 4;
            miCarro.anyoModelo = 2015;
            miCarro.precio = 200000;
            miCarro.miMotor.asignarNumeroCilindros(4);
            miCarro.miMotor.asignarTipoCarburador("Inyeccion");
            miCarro.miMotor.TipoCombustible= "Gasolina";
            miCarro.obtenerCaracteristicas();
            miCarro.miTanque.llenarTanque((decimal) 40.0);
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
            miCarro.frenar();
            miCarro.acelerar();
           
            Console.WriteLine("He terminado");
        }
    }
}
